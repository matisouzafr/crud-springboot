# Crud-Springboot Basic


## Descripción
Este proyecto esta basado en [Spring boot](https://spring.io/projects/spring-boot/).
Inicializado desde [spring initializr](https://start.spring.io/)

````
Proyecto con maven
Spring boot version 2.7.6
Packaging War
Java 11
````

## Dependencias

`Spring Web`: Para crear aplicaciones web, incluidas RESTful, utilizando Spring MVC. Utiliza Apache Tomcat .

`Spring Data MongoDB`: Almacene datos en documentos flexibles similares a JSON, lo que significa que los campos pueden variar de un documento a otro y la estructura de datos se puede cambiar con el tiempo.

`Lombok` : Biblioteca de anotaciones de Java que ayuda a reducir el código repetitivo, ejemplo: getters, setters y constructores


## Instalación
El proyecto se crea desde spring Init y luego se importa a su IDE, luego compilar el proyecto para resolver las dependencias


## Configuración de base de datos con Mongo Atlas
1. Entramos en Connect

![](asset/bj2XGiJmSM.png)

2. Seleccionamos Connect your application o podemos usar MongoDB compass (para esto se debe descargar mongo compass)

![](asset/chrome_z83odl6Xvt.png)

3. Seleccionamos Driver y version
4. Copiamos el contenido dentro de la caja

![](asset/chrome_70emPyggMh.png)

5. Nos dirigimos a aplication.properties
6. Agregamos spring.data.mongodb.uri= y luego pegamos lo copiado en el paso 4
7. Deberia verse asi:


![](asset/idea64_PnOwDtuGjP.png)

8. Por ultimo donde esta < username > y < password > lo quitamos, incluyendo los diamantes <> y agregamos nuestro usuario y contraseña.
9. Luego agregamos el nombre de nuestra base de datos (ENTRE / y el signo ?)  mongodb+srv://<username>:<password>@cluster0.esx0c6e.mongodb.net/`nombreBaseDeDatos`?retryWrites=true&w=majority


## Correr nuestra aplicación

con doble click en spring-boot:run y dirijase a [http://localhost:8080/api](http://localhost:8080/api)

![](asset/idea64_iqniTQ8dFu.png)