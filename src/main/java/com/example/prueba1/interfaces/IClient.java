package com.example.prueba1.interfaces;

import com.example.prueba1.domain.Client;

import java.util.Optional;

/**
 * Defino los metodos que voy a usar para interactuar con mi base de datos
 */
public interface IClient {

    Client saveClient(Client client);

    Optional<Client> findClientById(String id);

    Iterable<Client> findAllClients();

    void deleteClientById(String id);
}
