package com.example.prueba1.services;

import com.example.prueba1.domain.Client;
import com.example.prueba1.interfaces.IClient;
import com.example.prueba1.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @Service - mediante esta notación indicamos que la clase contiene todas la lógica de negocio
 * que será usada por otras capas como son los controladores.
 *
 *@IClient - Luego de implementar IClient, intellij nos pedira que implementemos sus metodos
 */
@Service
public class ClientServiceImpl implements IClient {

    /**
     * @Autowired - inyectamos una instancia de repositorio como dependencia de esta clase servicie,
     * la etiqueta se encargara en tiempo de ejecución de construir los objetos y enlazarlos entre los componentes.
     */
    @Autowired
    ClientRepository repository;

    /**
     * El Metodo saveClient lo usamos para guardar en la base de datos
     * con repository podemos hacer uso de los metodos para interactuar con la base de datos
     */
    @Override
    public Client saveClient(Client client) {
        return repository.save(client);
    }

    @Override
    public Optional<Client> findClientById(String id) {
        return repository.findById(id);
    }

    @Override
    public Iterable<Client> findAllClients() {
        return repository.findAll();
    }

    @Override
    public void deleteClientById(String id) {
        repository.deleteById(id);
    }
}
