package com.example.prueba1.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @Document - Permite identificar a la clase como un documento dentro de una colección en una base de datos de mongo,
 * de esta manera las instancias de esta clase serán los documentos que integren una colección determinada.
 * Por defecto el nombre de la colección será el nombre de la clase.
 */
@Document(collection = "clients")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    /**
     * @Id - definirá al atributo seleccionado como el identificador del documento,
     * aquel atributo que hace único el registro y que no se podrá modificar.
     */
    @Id
    private String id;
    private String nombre;
    private String apellido;
}
