package com.example.prueba1.controller;

import com.example.prueba1.domain.Client;
import com.example.prueba1.services.ClientServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @RestController Esta anotación se usa para crear los servicios web RESTful mapeando los datos de la solicitud al método de controlador definido.
 * Una vez que se genera el cuerpo de la respuesta a partir del método del controlador, lo convierte en una respuesta JSON o XML
 * <p>
 * @RequestMapping Mediante esta anotación podemos definir la ruta raiz de nuestro servicio,
 * en el ejemplo podemos acceder al api por medio de la url: http://localhost:8080/api
 */
@RestController
@RequestMapping("/api")
public class ClientController {

    @Autowired
    ClientServiceImpl service;

    /**
     * @Get/Post/Put/Delete Mapping
     * Definen rutas para cada una de las operaciones de un servicio web con su correspondiente método en HTTP (Get, Post, Put y Delete).
     * Mediante esta notación exponemos el tipo de funcionalidad que tendrá cada método.
     * <p>
     * <p>
     * @PathVariable permite obtener una variable que se envía dentro de la url de acceso al servicio.
     * por ejemplo si usamos en una petición GET http://localhost:8080/api/getClientById/60ca2f85b656c57ca1643a15 estaremos asignando un id para la consulta.
     * <p>
     * <p>
     * @RequestBody permite obtener un objeto en formato JSON,
     * es importante que el formato JSON enviado en la petición corresponda con la estructura de nuestro modelo Client
     */

    @GetMapping("/getClients")
    public Iterable<Client> findAllClients() {
        return service.findAllClients();
    }

    @GetMapping("/getClientById/{id}")
    public Optional<Client> findClientById(@PathVariable("id") String id) {
        return service.findClientById(id);
    }

    @PostMapping("/create")
    public Client saveClient(@RequestBody Client client) {
        return service.saveClient(client);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteClientById(@PathVariable("id") String id) {
        service.deleteClientById(id);
    }


    /**
     * Optional<Client> clientUpdated = service.findClientById(id) me devuelve el ciente que quiero actualizado,
     * clientUpdated.get() me devuelve el objeto Client, luego haciendo uso de los setter,
     * Le establezco el nuevo nombre que estoy obteniendo desde el body de la solicitud @RequestBody Client client.
     * Al final uso return service.saveClient(clientUpdated.get()) para guardar los nuevos datos actualizados.
     */
    @PutMapping("update/{id}")
    public Client update(@PathVariable("id") String id, @RequestBody Client client) {
        Optional<Client> clientUpdated = service.findClientById(id);

        clientUpdated.get().setNombre(client.getNombre());
        clientUpdated.get().setApellido(client.getApellido());

        return service.saveClient(clientUpdated.get());
    }
}
